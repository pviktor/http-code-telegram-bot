from telegram import *
from telegram.ext import *
import os
import logging

TOKEN = os.getenv('HTTPCODEBOT_TOKEN')

codes = { 100: "Continue"
        , 101: "Switching Protocols"
        , 102: "Processing"
        , 200: "OK"
        , 201: "Created"
        , 202: "Accepted"
        , 203: "Non-authoritative Information"
        , 204: "No Content"
        , 205: "Reset Content"
        , 206: "Partial Content"
        , 207: "Multi-Status"
        , 208: "Already Reported"
        , 226: "IM Used"
        , 300: "Multiple Choices"
        , 301: "Moved Permanently"
        , 302: "Found"
        , 303: "See Other"
        , 304: "Not Modified"
        , 305: "Use Proxy"
        , 307: "Temporary Redirect"
        , 308: "Permanent Redirect"
        , 400: "Bad Request"
        , 401: "Unauthorized"
        , 402: "Payment Required"
        , 403: "Forbidden"
        , 404: "Not Found"
        , 405: "Method Not Allowed"
        , 406: "Not Acceptable"
        , 407: "Proxy Authentication Required"
        , 408: "Request Timeout"
        , 409: "Conflict"
        , 410: "Gone"
        , 411: "Length Required"
        , 412: "Precondition Failed"
        , 413: "Payload Too Large"
        , 414: "Request-URI Too Long"
        , 415: "Unsupported Media Type"
        , 416: "Requested Range Not Satisfiable"
        , 417: "Expectation Failed"
        , 418: "I'm a teapot"
        , 421: "Misdirected Request"
        , 422: "Unprocessable Entity"
        , 423: "Locked"
        , 424: "Failed Dependency"
        , 426: "Upgrade Required"
        , 428: "Precondition Required"
        , 429: "Too Many Requests"
        , 431: "Request Header Fields Too Large"
        , 444: "Connection Closed Without Response"
        , 451: "Unavailable For Legal Reasons"
        , 499: "Client Closed Request"
        , 500: "Internal Server Error"
        , 501: "Not Implemented"
        , 502: "Bad Gateway"
        , 503: "Service Unavailable"
        , 504: "Gateway Timeout"
        , 505: "HTTP Version Not Supported"
        , 506: "Variant Also Negotiates"
        , 507: "Insufficient Storage"
        , 508: "Loop Detected"
        , 510: "Not Extended"
        , 511: "Network Authentication Required"
        , 599: "Network Connect Timeout Error" }


def inline_handler(u, c):
    tquery = u.inline_query.query
    from_user = u.inline_query.from_user
    query = tquery.strip()

    def not_valid():
        results = [
            InlineQueryResultArticle(
                id=from_user.id,
                title="Invalid code",
                input_message_content=InputTextMessageContent("The code is not valid!"),
            description="The code is not valid!")
        ]
        u.inline_query.answer(results, is_personal=True)

    
    code = 0
    try:
        code = int(query)
    except ValueError:
        not_valid()
        return

    response = ''
    try:
        response = f'{code}: {codes[code]}'
    except KeyError:
        not_valid()
        return

    try:
        results = [
            InlineQueryResultArticle(
                id=from_user.id,
                title=str(code),
                input_message_content=InputTextMessageContent(response),
            description=response)
        ]
        u.inline_query.answer(results)
    except Exception as ex:
        log.error(f'Cannot send: {ex}')


def init_log():
    logging.basicConfig(
        format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(funcName)s: %(message)s',
        datefmt='%H:%M:%S',
        level=logging.ERROR
    )

    global log
    log = logging.getLogger(__name__)

    logging.getLogger('pingdom').setLevel(logging.INFO)
    log.setLevel(logging.INFO)


def main():
    init_log()

    updater = Updater(TOKEN, use_context=True)
    dp = updater.dispatcher

    dp.add_handler(InlineQueryHandler(inline_handler))

    log.info('Starting polling')
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()

