# HTTP Code Telegram bot

The original version is t.me/httpcodebot

## Run
```shell
python3 -m venv venv
. venv/bin/activate
pip3 install -r requirements.txt
HTTPCODEBOT_TOKEN='your_bot_token' python3 bot.py
```

Run in docker:
```shell
docker build -t httpcodebot .
docker run -d -e HTTPCODEBOT_TOKEN='your_token' httpcodebot
```
